var ctx = document.getElementById('myChart3');
var myChart3 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        datasets: [{
            data: [1000, 2000, 3000, 2000, 3000, 4000, 1000, 3000, 6000, 8000, 5000],
            backgroundColor: ["#e3e9f2", "#e3e9f2", "#e3e9f2", "#e3e9f2", "#e3e9f2", "#e3e9f2", "#e3e9f2", "#e3e9f2", "#e3e9f2", "blue", "#e3e9f2"],

        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    display: false,
                },
                ticks: {
                    min: 0,
                    max: 8000,
                    stepSize: 2000,
                    fontColor: "#e3e9f2",
                    callback: function(label, index, labels) {
                        return label / 1000 + 'k';
                    }
                },
            }],
            xAxes: [{
                barThickness: 16,
                gridLines: {
                    drawBorder: false,
                    display: false,
                },
                ticks: {
                    display: false,
                }

            }],

        }
    }
});
var ctx = document.getElementById('myChart4');
var myChart4 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        datasets: [{
            data: [1000, 3000, 2000, 7000, 3000, 4000, 6000, 3000, 5000, 9000, 7000, 2000, 6000, 2000],
            backgroundColor: ["#e3e9f2", "#e3e9f2", "#e3e9f2", "#e3e9f2", "#3bd949", "3bd949", "3bd949", "3bd949", "3bd949", "3bd949", "3bd949", "3bd949", "3bd949", "3bd949"],

        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    display: false,
                },
                ticks: {
                    min: 0,
                    max: 8000,
                    stepSize: 2000,
                    fontColor: "#e3e9f2",
                    callback: function(label, index, labels) {
                        return label / 1000 + 'k';
                    }
                }
            }],
            xAxes: [{
                barThickness: 16,
                gridLines: {
                    drawBorder: false,
                    display: false,
                },
                ticks: {
                    display: false,
                }

            }],

        }
    }
});

var ctx = document.getElementById('myChart1');
var myChart1 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [2013, 2014, 2015, 2016, 2017, 2018],
        datasets: [{
            data: [1, 3, 5, 1, 4, 2],
            borderColor: "rgb(51, 51, 255)",
            backgroundColor: 'rgba(51, 51, 255,0.6)',
            pointRadius: 4,

        }, {
            data: [1, 6, 1, 6, 1, 2],
            borderColor: "rgb(255, 26, 117)",
            backgroundColor: 'rgba(255, 26, 117,0.6)',
            pointRadius: 4,

        }]

    },
    options: {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                //     ticks: {
                //    display: false,
                // }
            }],
            yAxes: [{
                //    gridLines: {
                // 		drawBorder: false,
                // 		display : false,
                // 	},

                ticks: {
                    display: false,
                    min: 0,
                    max: 7,
                    stepSize: 1
                }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }
});


var ctx = document.getElementById('myChart5');
var myChart5 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Mon', 'Tue', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun'],
        datasets: [{
            data: [200, 400, 300, 500, 400, 300, 200],
            backgroundColor: '#ffffff'

        }, {
            data: [600, 600, 600, 600, 600, 600, 600],
            backgroundColor: 'rgba(255, 255, 255,0.4)'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                gridLines: {
                    stacked: true,
                    drawBorder: false,
                    display: false,
                },
                ticks: {
                    min: 0,
                    max: 600,
                    stepSize: 100,
                    fontColor: "white",
                }
            }],
            xAxes: [{
                stacked: true,
                barThickness: 12,
                gridLines: {
                    drawBorder: false,
                    display: false,
                },
                ticks: {
                    fontColor: "white"
                }

            }],

        }
    }
});

$(function() {
    $('#world-map').vectorMap({
        map: 'world_mill_en',
        backgroundColor: '#FFFFFF',
        regionStyle: {
            initial: {
                fill: '#128da7'
            },
            hover: {
                fill: "#A0D1DC"
            }
        }

    });
});




// $('.remove').click(function() {
//     $(this).parent().parent().parent('div').remove();
// });


var btn = document.getElementsByClassName('remove')
for (var i = 0; i < btn.length; i++) {
    btn[i].addEventListener('click', function(e) {
        e.currentTarget.parentNode.parentNode.parentNode.remove();
    }, false);
}


function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(e) {
    if (!e.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

// function myFunction1() {
//     document.getElementById("myDropdown1").classList.toggle("show1");
// }

// window.onclick = function(e) {
//     if (!e.target.matches('.dropbtn1')) {
//         var dropdowns = document.getElementsByClassName("dropdown-content1");
//         var i;
//         for (i = 0; i < dropdowns.length; i++) {
//             var openDropdown = dropdowns[i];
//             if (openDropdown.classList.contains('show1')) {
//                 openDropdown.classList.remove('show1');
//             }
//         }
//     }
// }



$("#toggle-icon").click(function() {
    $("#mysidebar").hide();
    $(this).hide();
    $("#toggle-icon-open").show();
    $("#toggle-icon-open").css("display", "inline-block");
    $(".header-body-footer").addClass("width100");
});

$("#toggle-icon-open").click(function() {
    if ($(window).width() < 991) {
        $('.sidebar').attr('margin-left', '250px');
        alert("hello");
    }
    $("#mysidebar").show();
    $(this).hide();
    $(".open-menu").show();
    $(".header-body-footer").removeClass("width100");
    // if (window_width < 900)
    //     $('.sidebar').hide();
    // else
    //     $('.sidebar').show();
});